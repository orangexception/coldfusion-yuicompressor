# ColdFusion-YUICompressor with Railo Support
There are several YUICompressor implementations in ColdFusion. However, I have not found one that works in Railo.

This project was based off of Tom de Manincor's project on RIAForge, http://yuicompressorcfc.riaforge.org/.

For those interested, you can read this thread to find out why Tom's project doesn't work in Railo ( http://groups.google.com/group/javaloader-dev/browse_thread/thread/3cb3f56b7a84c704 ).

To use YUICompressor's JavaScriptCompressor in Railo you need a Java object of type ErrorReporter, which doesn't come packed with YUICompressor.

YUICompressor uses Rhino from Mozilla to implement JavaScript in Java. I downloaded the latest copy of Rhino from Mozilla and then I created org.mozilla.javascript.YUIJavaScriptCompressorErrorReporter.java based off of the YUICompressor inline ErrorReporter object.

I compiled and jar'd the class files into orangexception-yuicompressor.jar. (It's amusing how long it took me to actually do this part.)

The Compressor.cfc file first loads the YUICompressor jar file and then the orangexception-yuicompressor.jar. This allows Compressor.cfc to create a ErrorReporter object, which in turns allows Compressor.cfc to create a JavaScriptCompressor object.

I chose not to package YUICompressor in my jar file for easier upgrades in the future. This means that you can change the version of YUICompressor without having to recompile orangexception-yuicompressor.jar.

## ColdFusion-YUICompressor with Railo Support Source
This github project contains the source files for creating orangexception-yuicompressor.jar.

## YUI Compressor Source Files
https://github.com/yui/yuicompressor

## Mozilla's Rhino Source Files
https://github.com/mozilla/rhino