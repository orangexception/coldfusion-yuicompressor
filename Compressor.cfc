<cfcomponent	name=	"Compressor"
				output=	"false"
				hint=	"I compress things with YUICompressor">

	<cffunction	name=	"init"
				output=	"false"
				hint=	"I am the constructor">
		<cfargument	name=		"JavaLoader"
					required=	false
					default=	""
					hint=		"I am a reference to JavaLoader instantiated with my needs." />
		<cfscript>
		variables.sThisPath=	ExpandPath( "/lib/orangexception/yuicompressor/" );
		variables.asJarPaths=	[
									"#sThisPath#yuicompressor-2.4.6.jar" ,
									"#sThisPath#RailoYUICompressor.jar" ];

		//	Did you send me an existing JavaLoader?
		variables.JavaLoader=	arguments.JavaLoader;
		if(	IsObject( variables.JavaLoader ) == false ) {
			//	Does the server already have JavaLoader?
			if( StructKeyExists( SERVER , "JavaLoader" ) ) {
				variables.JavaLoader=	SERVER.JavaLoader;

			}
			else {
				//	I'll make an instance of JavaLoader
				variables.JavaLoader=
					CreateObject( "component" , "lib.javaloader.JavaLoader" ).init(
						asJarPaths );

			}

		}

		return	this;
		</cfscript>
	</cffunction>

	<cffunction	name=	"compress"
				output=	"false"
				hint=	"I compress thigns using YUICompressor">
		<cfargument	name=		"Value"
					required=	"false"
					default=	""
					hint=		"I am a String value you want to compress." />
		<cfargument	name=		"Type"
					required=	"false"
					default=	""
					hint=		"I the type of String you want to compress. I accept JavaScript, js, Stylesheet, and css." />

		<cfscript>
		var	jStringReader=
			CreateObject(	"java" ,
							"java.io.StringReader" ).init( Value );
		var	jStringWriter=
			CreateObject(	"java" ,
							"java.io.StringWriter" ).init();

		//	JavaScript Compression
		if( ListFindNoCase( "JavaScript,JS" , Type ) ) {

			//	Use Custom Railo Loader
			var	jRailoJavaScriptCompressor=
				JavaLoader.create( "lib.orangexception.assets.RailoJavaScriptCompressor" ).init( jStringReader );

			//	Compress JavaScript
			jRailoJavaScriptCompressor.compress(	jStringWriter ,
													JavaCast(	"int" ,		-1 ) ,
													JavaCast(	"boolean" ,	false ) ,
													JavaCast(	"boolean" ,	false ) ,
													JavaCast(	"boolean" ,	false ) ,
													JavaCast(	"boolean" ,	false ) );

		}
		//	Stylesheet Compression
		else if( ListFindNoCase( "Stylesheet,CSS" , Type ) ) {
			//	Standard YUI Compressor
			JavaLoader.create(
				"com.yahoo.platform.yui.compressor.CssCompressor" ).init(
						jStringReader ).compress(
							jStringWriter ,
							JavaCast(	"int" ,		-1 ) );

		}

		//	Send back jStringWriter's value as a string.
		return	jStringWriter.toString();
		</cfscript>
	</cffunction>

</cfcomponent>
